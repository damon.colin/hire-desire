Feature: Messages templatization

  Scenario: Build templatized message
    When I build message for template "0c7c3c56-7edc-468c-ba15-1d72e3d6cbd8" and default parameters
    Then I should have message "Bonsoir Colin, bienvenue sur HireDesire"