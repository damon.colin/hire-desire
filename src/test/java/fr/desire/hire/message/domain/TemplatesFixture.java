package fr.desire.hire.message.domain;

public final class TemplatesFixture {

  private TemplatesFixture() {}

  public static Context hireDesireContext() {
    return Context.builder().put("name", "Colin").put("service", "HireDesire").build();
  }

  public static Message message() {
    return new Message("Bonsoir Colin, bienvenue sur HireDesire");
  }
}
