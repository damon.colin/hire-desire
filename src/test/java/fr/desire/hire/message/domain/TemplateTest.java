package fr.desire.hire.message.domain;

import static fr.desire.hire.message.domain.TemplatesFixture.*;
import static org.assertj.core.api.Assertions.*;

import fr.desire.hire.UnitTest;
import org.junit.jupiter.api.Test;

@UnitTest
class TemplateTest {

  @Test
  void shouldBuildMessageWithoutParameters() {
    Template template = new Template("Bonsoir");

    assertThat(template.message(Context.EMPTY)).isEqualTo(new Message("Bonsoir"));
  }

  @Test
  void shouldBuildMessageWithParameters() {
    Template template = new Template("Bonsoir {{name}}, bienvenue sur {{ service }}");

    assertThat(template.message(hireDesireContext())).isEqualTo(message());
  }
}
