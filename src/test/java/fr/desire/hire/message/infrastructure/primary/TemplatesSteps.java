package fr.desire.hire.message.infrastructure.primary;

import static fr.desire.hire.cucumber.CucumberAssertions.*;

import fr.desire.hire.cucumber.CucumberRestTemplate;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class TemplatesSteps {

  private static final String DEFAULT_CONTEXT =
    """
    {
      "parameters": [
        {
          "key": "name",
          "value": "Colin"
        },
        {
          "key": "service",
          "value": "HireDesire"
        }
       ]
    }
    """;

  @Autowired
  private CucumberRestTemplate rest;

  @When("I build message for template {string} and default parameters")
  public void buildMessage(String template) {
    rest.post("/api/templates/" + template + "/messages", DEFAULT_CONTEXT);
  }

  @Then("I should have message {string}")
  public void shouldHaveMessage(String expectedMessage) {
    assertThatLastResponse().hasOkStatus().hasElement("$.content").withValue(expectedMessage);
  }
}
