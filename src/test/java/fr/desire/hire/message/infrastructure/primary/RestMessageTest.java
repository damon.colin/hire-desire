package fr.desire.hire.message.infrastructure.primary;

import static fr.desire.hire.message.domain.TemplatesFixture.*;
import static org.assertj.core.api.Assertions.*;

import fr.desire.hire.JsonHelper;
import fr.desire.hire.UnitTest;
import org.junit.jupiter.api.Test;

@UnitTest
class RestMessageTest {

  @Test
  void shouldSerializeToJson() {
    assertThat(JsonHelper.writeAsString(RestMessage.from(message()))).isEqualTo(json());
  }

  private String json() {
    return """
    {\
    "content":"Bonsoir Colin, bienvenue sur HireDesire"\
    }\
    """;
  }
}
