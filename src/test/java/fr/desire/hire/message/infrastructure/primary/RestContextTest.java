package fr.desire.hire.message.infrastructure.primary;

import static fr.desire.hire.BeanValidationAssertions.*;
import static fr.desire.hire.message.domain.TemplatesFixture.*;
import static org.assertj.core.api.Assertions.*;

import fr.desire.hire.JsonHelper;
import fr.desire.hire.UnitTest;
import org.junit.jupiter.api.Test;

@UnitTest
class RestContextTest {

  @Test
  void shouldDeserializeFromJson() {
    assertThat(JsonHelper.readFromJson(json(), RestContext.class).toDomain()).usingRecursiveComparison().isEqualTo(hireDesireContext());
  }

  private String json() {
    return """
      {
      "parameters": [
        {
          "key": "name",
          "value": "Colin"
        },
        {
          "key": "service",
          "value": "HireDesire"
        }
       ]
    }
    """;
  }

  @Test
  void shouldNotValidateContextWithoutParameters() {
    assertThatBean(new RestContext(null)).hasInvalidProperty("parameters");
  }
}
