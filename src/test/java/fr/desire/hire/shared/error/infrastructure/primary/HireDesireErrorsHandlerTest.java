package fr.desire.hire.shared.error.infrastructure.primary;

import static org.mockito.Mockito.*;

import ch.qos.logback.classic.Level;
import fr.desire.hire.Logs;
import fr.desire.hire.LogsSpy;
import fr.desire.hire.LogsSpyExtension;
import fr.desire.hire.UnitTest;
import fr.desire.hire.shared.error.domain.HireDesireException;
import fr.desire.hire.shared.error.domain.StandardErrorKey;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.context.MessageSource;

@UnitTest
@ExtendWith(LogsSpyExtension.class)
class HireDesireErrorsHandlerTest {

  private static final HireDesireErrorsHandler handler = new HireDesireErrorsHandler(mock(MessageSource.class));

  @Logs
  private LogsSpy logs;

  @Test
  void shouldLogServerErrorAsError() {
    handler.handleHireDesireException(
      HireDesireException.internalServerError(StandardErrorKey.INTERNAL_SERVER_ERROR).message("Oops").build()
    );

    logs.shouldHave(Level.ERROR, "Oops");
  }

  @Test
  void shouldLogClientErrorAsInfo() {
    handler.handleHireDesireException(HireDesireException.badRequest(StandardErrorKey.BAD_REQUEST).message("Oops").build());

    logs.shouldHave(Level.INFO, "Oops");
  }
}
