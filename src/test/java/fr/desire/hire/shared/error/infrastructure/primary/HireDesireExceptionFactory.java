package fr.desire.hire.shared.error.infrastructure.primary;

import fr.desire.hire.shared.error.domain.HireDesireException;

public final class HireDesireExceptionFactory {

  private HireDesireExceptionFactory() {}

  public static final HireDesireException buildEmptyException() {
    return HireDesireException.builder(null).build();
  }
}
