package fr.desire.hire.shared.error_generator.infrastructure.primary;

import fr.desire.hire.shared.error.domain.HireDesireException;
import fr.desire.hire.shared.error.domain.StandardErrorKey;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/errors")
class HireDesireErrorsResource {

  @GetMapping("bad-request")
  void getBadRequest() {
    throw HireDesireException.badRequest(StandardErrorKey.BAD_REQUEST).addParameter("code", "400").build();
  }
}
