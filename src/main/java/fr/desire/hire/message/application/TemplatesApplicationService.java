package fr.desire.hire.message.application;

import fr.desire.hire.message.domain.Context;
import fr.desire.hire.message.domain.Message;
import fr.desire.hire.message.domain.TemplateId;
import fr.desire.hire.message.domain.TemplatesRepository;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public class TemplatesApplicationService {

  private final TemplatesRepository templates;

  public TemplatesApplicationService(TemplatesRepository templates) {
    this.templates = templates;
  }

  public Optional<Message> message(TemplateId templateId, Context context) {
    return templates.get(templateId).map(template -> template.message(context));
  }
}
