package fr.desire.hire.message.domain;

import fr.desire.hire.shared.error.domain.Assert;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class Context {

  public static final Context EMPTY = builder().build();

  private final Collection<Parameter> parameters;

  private Context(ContextBuilder builder) {
    parameters = buildParameters(builder.parameters);
  }

  private Collection<Parameter> buildParameters(Map<String, String> parameters) {
    return parameters.entrySet().stream().map(entry -> new Parameter(entry.getKey(), entry.getValue())).toList();
  }

  public static ContextBuilder builder() {
    return new ContextBuilder();
  }

  public Stream<Parameter> parameters() {
    return parameters.stream();
  }

  public static class ContextBuilder {

    public final Map<String, String> parameters = new HashMap<>();

    private ContextBuilder() {}

    public ContextBuilder put(String key, String value) {
      Assert.notBlank("key", key);
      Assert.notBlank("value", value);

      parameters.put(key, value);

      return this;
    }

    public Context build() {
      return new Context(this);
    }
  }
}
