package fr.desire.hire.message.domain;

import fr.desire.hire.shared.error.domain.Assert;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

public record Template(String value) {
  private static final String MUSTACHE_START = "\\{\\{\\s*";
  private static final String MUSTACHE_END = "\\s*\\}\\}";

  public Template {
    Assert.notBlank("template", value);
  }

  public Message message(Context context) {
    Assert.notNull("context", context);

    String message = context.parameters().reduce(value(), replaceParameter(), keepFirst());

    return new Message(message);
  }

  private BiFunction<String, Parameter, String> replaceParameter() {
    return (result, parameter) -> result.replaceAll(parameterRegex(parameter), parameter.value());
  }

  private String parameterRegex(Parameter parameter) {
    return MUSTACHE_START + parameter.key() + MUSTACHE_END;
  }

  private BinaryOperator<String> keepFirst() {
    return (first, second) -> first;
  }
}
