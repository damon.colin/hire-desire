package fr.desire.hire.message.domain;

import java.util.Optional;

public interface TemplatesRepository {
  Optional<Template> get(TemplateId id);
}
