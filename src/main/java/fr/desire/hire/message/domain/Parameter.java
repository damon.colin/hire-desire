package fr.desire.hire.message.domain;

import fr.desire.hire.shared.error.domain.Assert;

public record Parameter(String key, String value) {
  public Parameter {
    Assert.notBlank("key", key);
    Assert.notBlank("value", value);
  }
}
