package fr.desire.hire.message.domain;

import fr.desire.hire.shared.error.domain.Assert;

public record Message(String value) {
  public Message {
    Assert.notBlank("message", value);
  }
}
