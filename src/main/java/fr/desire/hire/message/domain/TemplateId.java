package fr.desire.hire.message.domain;

import fr.desire.hire.shared.error.domain.Assert;
import java.util.UUID;

public record TemplateId(UUID value) {
  public TemplateId {
    Assert.notNull("templateId", value);
  }
}
