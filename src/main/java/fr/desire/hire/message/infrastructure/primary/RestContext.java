package fr.desire.hire.message.infrastructure.primary;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.desire.hire.message.domain.Context;
import fr.desire.hire.message.domain.Context.ContextBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;
import jakarta.validation.constraints.NotNull;
import java.util.Collection;

@Schema(name = "Context", description = "Context for a message")
class RestContext {

  private final Collection<RestParameter> parameters;

  RestContext(@JsonProperty("parameters") Collection<RestParameter> parameters) {
    this.parameters = parameters;
  }

  public Context toDomain() {
    ContextBuilder builder = Context.builder();

    parameters.stream().forEach(parameter -> builder.put(parameter.key(), parameter.value()));

    return builder.build();
  }

  @NotNull
  @Schema(description = "Parameters for the message", requiredMode = RequiredMode.REQUIRED)
  public Collection<RestParameter> getParameters() {
    return parameters;
  }
}
