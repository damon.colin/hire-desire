package fr.desire.hire.message.infrastructure.primary;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;

record RestParameter(
  @Schema(description = "Key of the parameter", requiredMode = RequiredMode.REQUIRED) String key,
  @Schema(description = "Value of the parameter", requiredMode = RequiredMode.REQUIRED) String value
) {}
