package fr.desire.hire.message.infrastructure.secondary;

import fr.desire.hire.message.domain.Template;
import fr.desire.hire.message.domain.TemplateId;
import fr.desire.hire.message.domain.TemplatesRepository;
import fr.desire.hire.shared.error.domain.Assert;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import org.springframework.stereotype.Repository;

@Repository
class InMemoryTemplatesRepository implements TemplatesRepository {

  private static final Map<TemplateId, Template> templates = buildTemplates();

  private static Map<TemplateId, Template> buildTemplates() {
    return Map.of(
      new TemplateId(UUID.fromString("0c7c3c56-7edc-468c-ba15-1d72e3d6cbd8")),
      new Template("Bonsoir {{name}}, bienvenue sur {{ service }}")
    );
  }

  @Override
  public Optional<Template> get(TemplateId id) {
    Assert.notNull("id", id);

    return Optional.ofNullable(templates.get(id));
  }
}
