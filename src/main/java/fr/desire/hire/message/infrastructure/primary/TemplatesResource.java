package fr.desire.hire.message.infrastructure.primary;

import fr.desire.hire.message.application.TemplatesApplicationService;
import fr.desire.hire.message.domain.TemplateId;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.UUID;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Tag(name = "Templates")
@RequestMapping("/api/templates")
class TemplatesResource {

  private final TemplatesApplicationService templates;

  public TemplatesResource(TemplatesApplicationService templates) {
    this.templates = templates;
  }

  @PostMapping("/{template-id}/messages")
  @Operation(description = "Generate a message")
  @ApiResponse(description = "Generated message", responseCode = "200")
  @ApiResponse(description = "If the template can't be found", responseCode = "404")
  ResponseEntity<RestMessage> buildMessage(
    @Schema(description = "ID of the template to use", requiredMode = RequiredMode.REQUIRED) @PathVariable("template-id") UUID templateId,
    @RequestBody @Validated RestContext context
  ) {
    return ResponseEntity.of(templates.message(new TemplateId(templateId), context.toDomain()).map(RestMessage::from));
  }
}
