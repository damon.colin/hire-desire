package fr.desire.hire.message.infrastructure.primary;

import fr.desire.hire.message.domain.Message;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;

@Schema(name = "Message", description = "Generated message")
class RestMessage {

  private final String content;

  private RestMessage(String content) {
    this.content = content;
  }

  static RestMessage from(Message message) {
    return new RestMessage(message.value());
  }

  @Schema(description = "Content of the message", requiredMode = RequiredMode.REQUIRED)
  public String getContent() {
    return content;
  }
}
